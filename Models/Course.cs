﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Course
    {
        [Key]
        public int CoursesId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
