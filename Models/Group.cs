﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Group
    {   [Key]
        public int Id { get; set; }
        public int Course_id { get; set; }
        public string Name { get; set; }
    }
}
