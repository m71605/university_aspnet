﻿using Infrastructure;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class AcessToCourses :IAcessToCourses
    {
        private readonly IUnitOfWork _unitOfWork;
        
        public AcessToCourses(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            PresetDatabase.Fill(_unitOfWork);
        }
        public  IEnumerable<Course> GetAll()
        {
            return _unitOfWork.Courses.GetAll();
        }

        public Course GetById(int id)
        {
            return _unitOfWork.Courses.GetAll().Where(u => u.CoursesId == id).First();
        }
    }
}
