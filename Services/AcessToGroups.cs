﻿using Infrastructure;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class AcessToGroups: IAcessToGroups
    {
        private readonly IUnitOfWork _unitOfWork;
        public AcessToGroups(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Group> GetByCourseId(int courseId)
        {
            return _unitOfWork.Groups.GetAll().Where(u => u.Course_id == courseId);
        }
        public Group GetById(int id)
        {
            return _unitOfWork.Groups.GetAll().Where(u => u.Id == id).First();
        }

        public void Update(Group group)
        {
            _unitOfWork.Groups.Update(group);
            _unitOfWork.Complete();
        }

        public bool IsGroupEmpty(int id)
        {
            return _unitOfWork.IsGroupEmpty(id);
        }

        public void Delete(Group group)
        {
            _unitOfWork.Groups.Remove(group);
            _unitOfWork.Complete();
        }

        public void Add(Group group)
        {
            _unitOfWork.Groups.Add(group);
            _unitOfWork.Complete();
        }

    }


    

   
    
}
