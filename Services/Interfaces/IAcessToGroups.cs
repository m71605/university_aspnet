﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IAcessToGroups
    {
        public IEnumerable<Group> GetByCourseId(int courseId);
        public Group GetById(int id);
        public void Update(Group group);
        public bool IsGroupEmpty(int id);
        public void Delete(Group group);
        public void Add(Group group);
        
    }
}
