﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IAcessToStudents
    {
        public Student GetById(int id);
        public void Update(Student student);
        public void Delete(Student student);
        public void Add(Student student);
        public IEnumerable<Student> GetAllByGroupId(int Groupid);
        
    }
}
