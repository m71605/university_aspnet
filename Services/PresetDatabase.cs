﻿using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Models;
using Microsoft.Extensions.Configuration;



namespace Services
{
    public static class PresetDatabase
    {
        public static void Configurate(IConfiguration configuration,IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<UniversityContext>(options =>
                     options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                     b => b.MigrationsAssembly(typeof(UniversityContext).Assembly.FullName)));
        }
         public static void Fill(IUnitOfWork unitOfWork)
        {
            bool isDatabaseEmpty = unitOfWork.isDatabaseEmpty();
           if (isDatabaseEmpty)
            {
               
                unitOfWork.Courses.AddRange(
                       new Course
                       {
                           Name = "Math",
                           Description = "Here you will study math"
                       },
                       new Course
                       {
                           Name = "Unity Gamedev",
                           Description = "Here you will study game development"
                       },
                       new Course
                       {
                           Name = "Painting",
                           Description = "Here you will study game development pictures"
                       },
                       new Course
                       {
                           Name = "Java Development",
                           Description = "Here you will study cooking"
                       }
                   );

                unitOfWork.Students.AddRange(
                    new Student
                       {
                        FirstName = "Ivan",
                        LastName = "Sidorov",
                        GroupId = 1
                        },
                       new Student
                        {
                            FirstName = "Petr",
                            LastName = "Gandi",
                            GroupId = 1
                        }
                     );
                unitOfWork.Groups.AddRange(
                    new Group
                    {
                        Name = "SR-01",
                        Course_id = 1
                    },
                        new Group
                        {
                            Name = "RS-07",
                            Course_id = 2
                        }
                     );
                unitOfWork.Complete();
            }
           
        }
    }
}
