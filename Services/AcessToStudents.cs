﻿using Infrastructure;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class AcessToStudents: IAcessToStudents
    {
        private readonly IUnitOfWork _unitOfWork;
        public AcessToStudents(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Student GetById(int id)
        {
            return _unitOfWork.Students.GetAll().Where(u => u.Id == id).Select(u => u).First();
        }
        public void Update(Student student)
        {
            _unitOfWork.Students.Update(student);
            _unitOfWork.Complete();
        }
        public void Delete(Student student)
        {
            _unitOfWork.Students.Remove(student);
            _unitOfWork.Complete();
        }
        public void Add(Student student)
        {
            _unitOfWork.Students.Add(student);
            _unitOfWork.Complete();
        }
        public IEnumerable<Student> GetAllByGroupId(int Groupid)
        {
            return _unitOfWork.Students.GetAll().Where(u => u.GroupId == Groupid);
        }
    }
}
