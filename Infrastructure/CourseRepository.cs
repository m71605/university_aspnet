﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure
{
    public class CourseRepository: GenericRepository<Course>, ICourseRepository
    {

        public CourseRepository(UniversityContext context) : base(context)
        {

        }
        public IEnumerable<Course> GetCourses()
        {
            return _context.Courses;
            
           // return _context.Courses.OrderByDescending(u => u.coursesId).Take(1).ToList();
        }
    }
}
