﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public interface IUnitOfWork: IDisposable
    {
        ICourseRepository Courses { get; }
        IGroupRepository Groups { get; }
        IStudentRepository Students { get; }
        int Complete();
        public bool IsGroupEmpty(int groupId);
        public bool isDatabaseEmpty();
    }
}
