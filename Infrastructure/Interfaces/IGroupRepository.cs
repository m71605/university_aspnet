﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public interface IGroupRepository : IGenericRepository<Group>
    {
    }
}
