﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public interface ICourseRepository : IGenericRepository<Course>
    {
        IEnumerable<Course> GetCourses();
    }
}
