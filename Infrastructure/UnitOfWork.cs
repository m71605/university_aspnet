﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly UniversityContext _context;
        public ICourseRepository Courses { get; }
        public IGroupRepository Groups { get; }
        public IStudentRepository Students { get; }



        public UnitOfWork(UniversityContext context)
        {
            _context = context;

            Courses = new CourseRepository(_context);

            Groups = new GroupRepository(_context);

            Students = new StudentRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public bool IsGroupEmpty(int groupId)
        {
            bool isGroupEmpty = !_context.Students.Where(p => p.GroupId == groupId).Select(u => u).Any();
            return isGroupEmpty;
        }
        public bool isDatabaseEmpty()
        {
            bool isEmpty = true;
            if (_context.Courses.Any()) isEmpty = false;
            if (_context.Students.Any()) isEmpty = false;
            if (_context.Groups.Any()) isEmpty = false;
            return isEmpty;
        }

    }
}
