﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;



namespace Infrastructure
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {

        public StudentRepository(UniversityContext context) : base(context)
        {

        }
    }
}
