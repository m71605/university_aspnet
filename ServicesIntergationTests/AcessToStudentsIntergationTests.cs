﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Infrastructure;
using Models;
using Services;

namespace ServicesIntergationTests
{
    public class AcessToStudentsIntergationTests
    {
        [Fact]
        public void GetById()
        {
            //arrange
            var unitOfWork = SetUoWAndDatabase(11);
            var acessToStudents = new AcessToStudents(unitOfWork);

            //act
            var student = acessToStudents.GetById(2);

            string expectedFirstName = "Student2";
            string expectedLastName = "LastName2";
            int expectedGroupId = 1;
            //assert
            
            Assert.Equal(student.FirstName, expectedFirstName);
            Assert.Equal(student.LastName, expectedLastName);
            Assert.Equal(student.GroupId, expectedGroupId);

        }
        [Fact]
        public void Update_Test()
        {
            //arrange
            var unitOfWork = SetUoWAndDatabase(12);
            var acessToStudents = new AcessToStudents(unitOfWork);
            //act
            var student = acessToStudents.GetById(2);
            student.FirstName = "Mahatma";
            student.LastName = "Gandi";
            student.GroupId = 3;

            acessToStudents.Update(student);

            var updatedSudent = acessToStudents.GetById(2);

            string expectedFirstName = "Mahatma";
            string expectedLastName = "Gandi";
            int expectedGroupId = 3;
            //assert
            Assert.Equal(updatedSudent.FirstName, expectedFirstName);
            Assert.Equal(updatedSudent.LastName, expectedLastName);
            Assert.Equal(updatedSudent.GroupId, expectedGroupId);
        }
        [Fact]
        public void Add_Test()
        {
            //arrange
            var unitOfWork = SetUoWAndDatabase(13);
            var acessToStudents = new AcessToStudents(unitOfWork);
            //act
            var newStudent = new Student();
            newStudent.Id = 5;
            newStudent.FirstName = "NewFirstName";
            newStudent.LastName = "NewLastName";
            newStudent.GroupId = 3;

            acessToStudents.Add(newStudent);
            var student = acessToStudents.GetById(5);


            string expectedFirstName = "NewFirstName";
            string expectedLastName = "NewLastName";
            int expectedGroupId = 3;
            //assert
            Assert.Equal(student.FirstName, expectedFirstName);
            Assert.Equal(student.LastName, expectedLastName);
            Assert.Equal(student.GroupId, expectedGroupId);
        }
        [Fact]
        public void Delete_Test()
        {
            //arrange
            var unitOfWork = SetUoWAndDatabase(14);
            var acessToStudents = new AcessToStudents(unitOfWork);
            //act
            var student = acessToStudents.GetById(1);

            acessToStudents.Delete(student);

            var students = acessToStudents.GetAllByGroupId(2).ToList();
            int expectedCount = 0;

            //assert
            Assert.Equal(students.Count, expectedCount);

        }
        [Fact]
        public void GetAllByGroupId_Test()
        {
            //arrange
            var unitOfWork = SetUoWAndDatabase(15);
            var acessToStudents = new AcessToStudents(unitOfWork);

            var students = acessToStudents.GetAllByGroupId(1).ToList();

            int expectedCount = 3;
            //assert
            Assert.Equal(students.Count, expectedCount);
        }

        private UnitOfWork SetUoWAndDatabase(int number)
        {
            var options = new DbContextOptionsBuilder<UniversityContext>()
         .UseInMemoryDatabase(databaseName: "universityDB" + Convert.ToString(number))
         .Options;

            using (var context = new UniversityContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                context.Students.Add(new Student { Id = 1, FirstName = "Student1", LastName = "LastName1", GroupId = 2 });
                context.Students.Add(new Student { Id = 2, FirstName = "Student2", LastName = "LastName2", GroupId = 1 });
                context.Students.Add(new Student { Id = 3, FirstName = "Student3", LastName = "LastName3", GroupId = 1 });
                context.Students.Add(new Student { Id = 4, FirstName = "Student4", LastName = "LastName4", GroupId = 1 });
                context.SaveChanges();
                context.Dispose();
            }
            return new UnitOfWork(new UniversityContext(options));
        }


        
    }
}
