﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Infrastructure;
using Models;
using Services;

namespace ServicesIntergationTests
{
   
    public class AcessToGroupsInterationTests
    {
        [Fact]
        public void GetByCourseId()
        {
            //arrange
            var unitOfWork = SetDatabase(1);
            var acessToGroups = new AcessToGroups(unitOfWork);
            var groups = acessToGroups.GetByCourseId(1).ToList();

            int expectedCount = 2;
            string expectedNameNumber0 = "SR-0";
            string expectedNameNumber1 = "SR-2";
            //assert
            Assert.Equal(groups.Count, expectedCount);
            Assert.Equal(groups[0].Name, expectedNameNumber0);
            Assert.Equal(groups[1].Name, expectedNameNumber1);
        }
        [Fact]
        public void GetById_Test()
        {
            //arrange
            var unitOfWork = SetDatabase(2);
            var acessToGroups = new AcessToGroups(unitOfWork);

            //act
            var group = acessToGroups.GetById(2);

            string expectedName = "SR-1";
            int expectedCourseId = 2;
           
            //assert
            Assert.Equal(group.Name, expectedName);
            Assert.Equal(group.Course_id, expectedCourseId);
        }
        [Fact]
        public void Update_Test()
        {
            //arrange
            var unitOfWork = SetDatabase(3);
            var acessToGroups = new AcessToGroups(unitOfWork);
            //act
            var group = acessToGroups.GetById(2);
            group.Name = "VVV-7";
            group.Course_id = 3;
            acessToGroups.Update(group);
            group = acessToGroups.GetById(2);

            string expectedName = "VVV-7";
            int expectedCourseId = 3;
            //assert
            Assert.Equal(group.Name, expectedName);
            Assert.Equal(group.Course_id, expectedCourseId);
        }
        [Fact]
        public void Add_Test()
        {
            //arrange
            var unitOfWork = SetDatabase(4);
            var acessToGroups = new AcessToGroups(unitOfWork);
            //act
            var newGroup = new Group();
            newGroup.Id = 4;
            newGroup.Name = "VVV-0";
            newGroup.Course_id = 1;
            acessToGroups.Add(newGroup);
            var group= acessToGroups.GetById(4);


            string expectedName = "VVV-0";
            int expectedCourseId = 1;
            //assert
            Assert.Equal(group.Name, expectedName);
            Assert.Equal(group.Course_id, expectedCourseId);
        }
        [Fact]
        public void Delete_Test()
        {
            //arrange
            var unitOfWork = SetDatabase(5);
            var acessToGroups = new AcessToGroups(unitOfWork);
            //act
          
            
            var group = acessToGroups.GetById(3);
            acessToGroups.Delete(group);
            var groups = acessToGroups.GetByCourseId(1).ToList();
            int expectedCount = 1;

            //assert
            Assert.Equal(groups.Count, expectedCount);
           
        }

        [Fact]
        public void IsGroupEmpty_Test()
        {
            //arrange
           var options = new DbContextOptionsBuilder<UniversityContext>()
         .UseInMemoryDatabase(databaseName: "universityDB")
         .Options;

            using (var context = new UniversityContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                context.Groups.Add(new Group { Id = 1, Name = "SR-0", Course_id = 1 });
                context.Groups.Add(new Group { Id = 2, Name = "SR-1", Course_id = 2 });
                context.Groups.Add(new Group { Id = 3, Name = "SR-2", Course_id = 1 });
                context.Students.Add(new Student { Id = 1, FirstName = "Ivan", LastName = "Petrov", GroupId = 2 });

                context.SaveChanges();
                context.Dispose();
            }
            var unitOfWork= new UnitOfWork(new UniversityContext(options));
            var acessToGroups = new AcessToGroups(unitOfWork);
            //act
            bool isGropEmpty1 = acessToGroups.IsGroupEmpty(1);
            bool isGropEmpty2 = acessToGroups.IsGroupEmpty(2);
            bool isGropEmpty3 = acessToGroups.IsGroupEmpty(3);

            //assert
            Assert.True(isGropEmpty1);
            Assert.False(isGropEmpty2);
            Assert.True(isGropEmpty3);

        }
        private UnitOfWork SetDatabase(int number)
        {
            var options = new DbContextOptionsBuilder<UniversityContext>()
         .UseInMemoryDatabase(databaseName: "universityDB" + Convert.ToString(number))
         .Options;

            using (var context = new UniversityContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                context.Groups.Add(new Group { Id = 1, Name = "SR-0", Course_id = 1 });
                context.Groups.Add(new Group { Id = 2, Name = "SR-1", Course_id = 2 });
                context.Groups.Add(new Group { Id = 3, Name = "SR-2", Course_id = 1 });
                context.SaveChanges();
                context.Dispose();
            }
            return new UnitOfWork(new UniversityContext(options));
        }

    }
}
