﻿using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Models;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Linq;

namespace ServicesIntergationTests
{

    
    public class AcessToCoursesInergationTests
    {
        [Fact]
        public void GetAll_Test()
        {
            //arrange
            var options = new DbContextOptionsBuilder<UniversityContext>()
          .UseInMemoryDatabase(databaseName: "universityDB")
          .Options;

            using (var context = new UniversityContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                context.Courses.Add(new Course { CoursesId = 1, Name = "Course1", Description = "none1" });
                context.Courses.Add(new Course { CoursesId = 2, Name = "Course2", Description = "none2" });
                context.SaveChanges();
                context.Dispose();
            }

            //act
            var unitOfWork = new UnitOfWork(new UniversityContext(options));
            var acessToCourses = new AcessToCourses(unitOfWork);
            var courses = acessToCourses.GetAll().ToList();
            int expectedCount = 2;
            string expectedNameNumber0 = "Course1";
            string expectedNameNumber1 = "Course2";
            //assert
            Assert.Equal(courses.Count, expectedCount);
            Assert.Equal(courses[0].Name, expectedNameNumber0);
            Assert.Equal(courses[1].Name, expectedNameNumber1);
           
        }

        [Fact]
        public void GetById_Test()
        {
            //arrange
            var options = new DbContextOptionsBuilder<UniversityContext>()
          .UseInMemoryDatabase(databaseName: "universityDB")
          .Options;

            using (var context = new UniversityContext(options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                context.Courses.Add(new Course { CoursesId = 1, Name = "Course1", Description = "none1" });
                context.Courses.Add(new Course { CoursesId = 2, Name = "Course2", Description = "none2" });
                context.SaveChanges();
                context.Dispose();
            }
            //act
            var unitOfWork = new UnitOfWork(new UniversityContext(options));
            var acessToCourses = new AcessToCourses(unitOfWork);
            var course = acessToCourses.GetById(2);
            
            string expectedName = "Course2";
            string expectedDescription = "none2";

            //assert
            Assert.Equal(course.Name, expectedName);
            Assert.Equal(course.Description, expectedDescription);
        }


    }
}
