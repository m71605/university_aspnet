﻿using Services.Interfaces;
using Microsoft.AspNetCore.Mvc;


namespace University.Controllers
{
    public class CourseController : Controller
    {
        private readonly IAcessToCourses _acessToCourses;
        public CourseController(IAcessToCourses acessToCourses)
        {
            _acessToCourses = acessToCourses;
        }
        public IActionResult Index()
        {
            var courses = _acessToCourses.GetAll();
            return View(courses);

        }
    }
}
