﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;



namespace University.Controllers
{
    
    public class StudentController : Controller
    {
        const string SAVE_VALUE = "save";
        const string DELETE_VALUE = "delete";

        private readonly IAcessToStudents _acessToStudents;
        private readonly IAcessToGroups _acessToGroups;
        public StudentController(IAcessToStudents acessToStudents, IAcessToGroups acessToGroups)
        {
            _acessToStudents = acessToStudents;
            _acessToGroups = acessToGroups;
        }
        [HttpGet]
        public IActionResult EditStudent(int id)
        {
            var student = _acessToStudents.GetById(id);
            ViewBag.FirstName = student.FirstName;
            ViewBag.LastName = student.LastName;
            ViewBag.StudentId = id;
            return View();
        }
        [HttpPost]
        public ActionResult ChangeStudentName(int studentid, string newFirstName, string newLastName, string action)
        {
            var student = _acessToStudents.GetById(studentid);
            var groupId = student.GroupId;
            if (action == SAVE_VALUE)
            {
                student.FirstName = newFirstName;
                student.LastName = newLastName;
                _acessToStudents.Update(student);
            }
            else if (action == DELETE_VALUE)
            {
                _acessToStudents.Delete(student);
            }
            return RedirectToAction("ShowStudents", "Student", new { @id = groupId });
        }
        [HttpPost]
        public ActionResult AddNewStudent(int groupid, string newFirstName, string newLastName, string action)
        {
            if (action == SAVE_VALUE)
            {
                Student student = new Student();
                student.FirstName = newFirstName;
                student.LastName = newLastName;
                student.GroupId = groupid;
                _acessToStudents.Add(student);
            }
            return RedirectToAction("ShowStudents", "Student", new { @id = groupid });
        }
        public IActionResult AddStudent(int id)
        {
            ViewBag.GroupId = id;
            return View();
        }
        public IActionResult ShowStudents(int id)
        {
            ViewBag.groupId = id;
            var students = _acessToStudents.GetAllByGroupId(id);
            var group = _acessToGroups.GetById(id);   
            ViewBag.courseId = group.Course_id;
            ViewBag.GroupName = group.Name;
            return View(students);
        }
    }
}
