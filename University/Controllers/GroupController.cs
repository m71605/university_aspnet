﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University.Controllers
{  
    public class GroupController : Controller
    {
        const string SAVE_VALUE = "save";
        const string DELETE_VALUE = "delete";
        const string SUCCESS_RENAMED_MESSAGE = "Group successfully remaned";
        const string SUCCESS_DELETED_MESSAGE = "Group successfully deleted";
        const string UNSUCCESS_DELETED_MESSAGE = "Group cannot be deleted cause it is not empty";

        private readonly IAcessToGroups _acessToGroups;
        private readonly IAcessToCourses _acessToCourses;
        public GroupController(IAcessToGroups acessToGroups, IAcessToCourses acessToCourses)
        {
            _acessToGroups = acessToGroups;
            _acessToCourses = acessToCourses;
        }
        [HttpGet]
        public IActionResult ShowGroups(int id, string message)
        {
            var groups = _acessToGroups.GetByCourseId(id);
            var course = _acessToCourses.GetById(id);
            ViewBag.CourseName = course.Name;
            if (message != null)
            {
                ViewBag.Message = message;
            }
            else
            {
                ViewBag.Message = "";
            }
            return View(groups);
        }
        [HttpGet]
        public IActionResult EditGroup(int id)
        {
            var group = _acessToGroups.GetById(id);
            ViewBag.GroupName = group.Name;
            ViewBag.GroupId = id;
            return View();
        }

        [HttpGet]
        public IActionResult AddGroup(int id)
        {
            ViewBag.CourseId = id;
            var course = _acessToCourses.GetById(id);
            ViewBag.CourseName = course.Name;
            return View();
        }

        [HttpPost]
        public ActionResult ChangeGroupName(int groupId, string newGroupName, string action)
        {
            var group = _acessToGroups.GetById(groupId); 
            int courseId = group.Course_id;

            if (action == SAVE_VALUE)
            {
                group.Name = newGroupName;
                _acessToGroups.Update(group);
            }
            else if (action == DELETE_VALUE)
            {
                return RedirectToAction("DeleteGroup", "Group", new { @id = groupId});
            }
            return RedirectToAction("ShowGroups", "Group", new { @id = courseId , @message = SUCCESS_RENAMED_MESSAGE });

        }
        public IActionResult DeleteGroup(int id)
        {
            var group = _acessToGroups.GetById(id); 
            bool isGroupEmpty = _acessToGroups.IsGroupEmpty(id);
            int courseId = group.Course_id;
            if (isGroupEmpty)
            {
                _acessToGroups.Delete(group);
            }
            else
            {
                return RedirectToAction("ShowGroups", "Group", new { @id = courseId, @message = UNSUCCESS_DELETED_MESSAGE });
            }
            return RedirectToAction("ShowGroups", "Group", new { @id = courseId, @message = SUCCESS_DELETED_MESSAGE });
        }


        [HttpPost]
        public ActionResult AddNewGroup(int courseId, string newGroupName, string action)
        {
            if (action == SAVE_VALUE)
            {
                Group group = new Group();
                group.Course_id = courseId;
                group.Name = newGroupName;
                _acessToGroups.Add(group);
             }
           
            return RedirectToAction("ShowGroups", "Group", new { @id = courseId });

        }
    }
    }
