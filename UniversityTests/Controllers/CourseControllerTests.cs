﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using University.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Xunit;
using Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Linq;

namespace University.Controllers.Tests
{
    
    public class CourseControllerTests
    {
        [Fact]
        public void IndexTest()
        {
            //arrange
            var mock = new Mock<IAcessToCourses>();
            mock.Setup(repo => repo.GetAll()).Returns(GetTestCourses());
            var controller = new CourseController(mock.Object);
            //act
            var result = controller.Index();
            //assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Course>>(
                viewResult.Model);
            int actual = model.Count();
            int expected = GetTestCourses().Count();
            Assert.Equal(expected, actual);
            
        }
        private IEnumerable<Course> GetTestCourses()
        {
            IEnumerable<Course> courses = new List<Course>
            {
                new Course { CoursesId =1, Name="Math", Description = "Empty"},
                new Course { CoursesId =2, Name="GameDev", Description = "Empty"}
            };
            return courses;
        }
    }
}