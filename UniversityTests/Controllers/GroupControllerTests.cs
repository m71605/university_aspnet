﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using Services.Interfaces;
using University.Controllers;
using Models;

namespace UniversityTests.Controllers
{
    public class GroupControllerTests
    {
        [Fact]
        public void ShowGroups_Test()
        {
            //arrange
            //var mock = new Mock<IAcessToGroups>();
            //mock.Setup(repo => repo.GetByCourseId).Returns(GetTestGroups());

           // var controller = new GroupController(mock.Object);
            //act

            //assert
        }
        private IEnumerable<Group> GetTestGroups()
        {
            IEnumerable<Group> groups = new List<Group>
            {
                new Group { Id =1, Name="SR-01", Course_id =1},
                new Group { Id =2, Name="SR-02", Course_id =2},
                new Group { Id =3, Name="SR-03", Course_id =1}
            };
            return groups;
        }
    }
}
